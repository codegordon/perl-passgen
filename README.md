# Installation

Copy or symlink *passgen* to a location in your system's PATH.
A good location on Linux systems is `/usr/local/bin`.
Ensure that *passgen* is marked as executable.

# Usage

```
$ passgen [length] [set]
```

`length` defaults to 20, and must be no larger than 64.
While there is no lower limit, a sufficiently long password will be more useful.

`set` defaults to `alpha`, and may be one of the following (see `man tr` for details):

- `alnum`
- `alpha`
- `digit`
- `graph`
- `lower`
- `punct`
- `upper`
- `xdigit`

# License

This project is released using the MIT license.