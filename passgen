#!/usr/bin/perl
use strict;
use warnings;

helptext() if (@ARGV && (($ARGV[0] eq '-h') || ($ARGV[0] eq '--help') || ($ARGV[0] eq 'help')));
licensetext() if (@ARGV && (($ARGV[0] eq '-l') || ($ARGV[0] eq '--license') || ($ARGV[0] eq 'license')));

# Constants
my $CRYPTO_LEN = $ARGV[0] || 20;
my $CRYPTO_BIT = $CRYPTO_LEN * 8;
my $CRYPTO_SET = $ARGV[1] || 'alnum';
my $CRYPTO_SET_LIST = {
	'alnum' => 1,
	'alpha' => 2,
	'digit' => 4,
	'graph' => 1,
	'lower' => 2,
	'punct' => 1,
	'upper' => 2,
	'xdigit' => 2
};

# Sanitize values
$CRYPTO_LEN = int($CRYPTO_LEN);
$CRYPTO_BIT = int($CRYPTO_BIT);
$CRYPTO_SET = lc($CRYPTO_SET);

# Check for valid values
die('Error: Password length cannot be larger than 64.') if ($CRYPTO_LEN > 64);
die('Error: Password translation set is invalid.') if (!$CRYPTO_SET_LIST->{$CRYPTO_SET});
$CRYPTO_LEN = 20 if ($CRYPTO_LEN < 1);

# Adjust bit size
$CRYPTO_BIT *= int($CRYPTO_SET_LIST->{$CRYPTO_SET});

# Get new crypto key
my $cmd = "head -c $CRYPTO_BIT /dev/urandom | tr -dc '[:$CRYPTO_SET:]' | fold -w $CRYPTO_LEN | head -n 1";
my $key = qx($cmd);
chomp($key);

die('Error: Password was not properly generated.') if (length($key) != $CRYPTO_LEN);
print $key."\n";
exit;

#
#
#
sub helptext {
	print <<"EOL";
Usage: passgen [length] [set]
    length          Length of the password to be generated; defaults to 20
    set             Translation set to use; defaults to "alnum"
                        - alnum, alpha, digit, graph, lower, punct, upper, xdigit
EOL
	exit;
}
sub licensetext {
	print <<"EOL";
MIT License

Copyright (c) 2017 Zachary Sheets

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
EOL
	exit;
}
